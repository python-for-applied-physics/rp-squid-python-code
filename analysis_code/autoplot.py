import numpy as np
import os
import matplotlib.pyplot as plt
from glob import glob
from IPython.core.display import HTML,display
import datetime

def plot_V_I(file):
    t,i,v = np.loadtxt(file, unpack=True)
    plt.subplots(figsize=(14,4))
    plt.subplot(121)
    plt.plot(t,v)
    plt.plot(t,i)
    plt.xlabel("Time (s)")
    plt.ylabel("I, V ($\mu$A, $\mu$V)")
    plt.axhline(0,ls=':',c='grey')
    plt.subplot(122)
    plt.plot(i,v)
    plt.axhline(0,ls=':',c='grey')
    plt.axvline(0,ls=':',c='grey')
    plt.xlabel("Applied current $I$ ($\mu$A)")
    plt.ylabel("Measured voltage $V$ ($\mu$A)")

def plot_R_T(file):
    R,T,tm,Iavg,Iptp,Vavg,Vptp,sigma_R = np.loadtxt(file, unpack=True)
    plt.subplots(figsize=(12,10))
    plt.subplot(221)
    plt.plot(tm,R)
    plt.plot(tm,R-sigma_R, ls=":",c="grey")
    plt.plot(tm,R+sigma_R, ls=":",c="grey")
    plt.axhline(0,ls=':',c='grey')
    plt.xlabel("Measurement Time $t_m$ (s)")
    plt.ylabel("Measured Resistance $R$ (ohms) and error margin")
    plt.subplot(222)
    plt.plot(tm,T)
    plt.axhline(0,ls=':',c='grey')
    plt.xlabel("Measurement Time $t_m$ (s)")
    plt.ylabel("Measured Temperature (K)")
    plt.subplot(223)
    for name in "Iavg","Iptp":
        d = eval(name)
        plt.plot(tm,d,label=name)
    plt.axhline(0,ls=':',c='grey')
    plt.xlabel("Measurement Time $t_m$ (s)")
    plt.ylabel("Iavg,Iptp ($\mu$A)")
    plt.legend()
    plt.subplot(224)
    plt.plot(T,R)
    plt.ylabel("Measured Resistance $R$ (ohms)")
    plt.xlabel("Measured Temperature (K)")

def plot_V_Phi(file):
    t,i,v = np.loadtxt(file, unpack=True)
    plt.subplots(figsize=(14,4))
    plt.subplot(121)
    plt.plot(t,v)
    plt.plot(t,i)
    plt.xlabel("Time (s)")
    plt.ylabel("I, V ($\mu$A, $\mu$V)")
    plt.axhline(0,ls=':',c='grey')
    plt.subplot(122)
    plt.plot(i,v)
    plt.axhline(0,ls=':',c='grey')
    plt.axvline(0,ls=':',c='grey')
    plt.xlabel("Flux bias current $I$ ($\mu$A)")
    plt.ylabel("Measured voltage $V$ ($\mu$A)")

def plot_PSD(file):
    f,Ipsd,Vpsd = np.loadtxt(file, unpack=True)
    plt.subplots(figsize=(14,6))
    plt.plot(f,Ipsd, label='Ipsd')
    plt.plot(f,Vpsd, label='Vpsd')
    plt.xlabel("Frequency (Hz)")
    plt.ylabel("Ipsd ($\mu$A$^2$/Hz), Vpsd ($\mu$V$^2$/Hz)")
    plt.legend()
    plt.yscale('log')
    plt.grid()

def meas_time(name):
    timestamp = "_".join(name.split('_')[-3:]).split(".")[0]
    return datetime.datetime.strptime(timestamp, "%Y-%m-%d-%H_%M_%S")


def plot_all_data():
    dat_files = glob("*.dat")
    dat_files.sort(key=meas_time)
    for file in dat_files:
        if "R_vs_T" in file:
            cmd=f"plot_R_T('{file}')"
        elif "V-I" in file:
            cmd=f"plot_V_I('{file}')"
        elif "V-Phi" in file:
            cmd=f"plot_V_Phi('{file}')"
        elif "PSD" in file:
            cmd=f"plot_PSD('{file}')"
        display(HTML("<h2> %s </h2>" % file))
        print(cmd)
        eval(cmd)
        plt.show()


def make_latex_folder():
    output_folder = "plot_all_my_data_latex"
    if not os.path.isdir(output_folder):
           os.makedirs(output_folder)
    latex_filename = output_folder + "/" + "plot_all_data.tex"
    latex_file = open(latex_filename, "w")
    dat_files = glob("*.dat")
    dat_files.sort(key=meas_time)
    for file in dat_files:
        if "R_vs_T" in file:
            cmd=f"plot_R_T('{file}')"
        elif "V-I" in file:
            cmd=f"plot_V_I('{file}')"
        elif "V-Phi" in file:
            cmd=f"plot_V_Phi('{file}')"
        elif "PSD" in file:
            cmd=f"plot_PSD('{file}')"
        eval(cmd)
        plt.title(file)
        outfile = output_folder + "/" +file+".png"
        plt.savefig(outfile, dpi=150)
        print(file)
        plt.close()
        latex_cmd = r"\includegraphics[width=0.5\textwidth]{%s}" % outfile + "\n"
        latex_file.write(latex_cmd)
        #display(HTML("<h2> %s </h2>" % file))
    latex_file.close()
    print("\n")
    print("Instructions:\nCopy the folder plot_all_my_data_latex into your\n"+
          "latex source folder then add the command:\n"
          "\n\\input{plot_all_my_data_latex/plot_all_data.tex}\n\n"
          "in your report latex file.")

# +
#make_latex_folder()

# +
#plot_all_data()

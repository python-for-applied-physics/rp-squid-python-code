---
jupyter:
  jupytext:
    formats: ipynb,md
    text_representation:
      extension: .md
      format_name: markdown
      format_version: '1.3'
      jupytext_version: 1.13.6
  kernelspec:
    display_name: Python 3 (ipykernel)
    language: python
    name: python3
---

<!-- #region -->
# TN2953-P SQUID Practicum Webpage

<img src="figs/MrSQUID_Chip.png" width=50%>


On this webpage, you will find information about the 2nd year undergraduate SQUID practicum (experimental lab) hosted by the Quantum Nanoscience (http://qn.tudelft.nl) department, run by the group of Prof. Gary Steele (http://steelelab.tudelft.nl). 

In this practicum, you will perform electrical measurements on a Superconducting Quantum Interference Device (SQUID, pictured above). You will observe the zero-resistance state of a superconductor,  the transition from superconducting to normal-metal behavior, and the consequences of the quantum interference of electron wavefunctions in the device. 


## General information

Read this first:

<a href="Information%20about%20TN2513-P%20Squid%20Practicum.html">Information about TN2513-P Squid Practicum</a>

## Manual

The manual for this practicum, for reference when you are preparing for, and doing, the experiments: 

<a href="TN2513-P%20SQUID%20Practicum%20Manual.html">Instruction manual</a>


## Instructional Videos

You can find them linked here:

<a href="Instructional Videos SQUID Practicum.html">Instructional Videos SQUID Practicum</a>

## Report writing guidelines and advice

Some guidelines and advice we have collected over the years:

<a href="Report%20Writing%20Guidelines%20and%20Advice.html">Report Writing Guidelines and Advice</a>

<br> 
<br> 

*You can find the source code to these documents and our control software in the <a href=https://gitlab.tudelft.nl/python-for-applied-physics/rp-squid-python-code>public repository</a> in the TU Delft Gitlab.* 
<!-- #endregion -->

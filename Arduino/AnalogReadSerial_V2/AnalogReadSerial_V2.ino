
void setup() {
  // initialize serial communication at 115200 or 9600 bits per second:
  Serial.begin(9600);
}

void loop() {
  if(Serial.available()>0){
    char userinput = Serial.read();

    if(userinput == 'g'){
      int sensorValue = analogRead(A3);
      Serial.println(sensorValue);
      delay(100);        // delay in between reads for stability
    }
  } 
}

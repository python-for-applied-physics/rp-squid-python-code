jupyter nbconvert --to html "SQUID Practicum.ipynb" --output index.html
jupyter nbconvert --to html "TN2513-P SQUID Practicum Manual.ipynb"
jupyter nbconvert --to html "Information about TN2513-P Squid Practicum.ipynb"
jupyter nbconvert --to html "Report Writing Guidelines and Advice.ipynb"
jupyter nbconvert --to html "Instructional Videos SQUID Practicum.ipynb"
echo "Last updated on $(date)" >> index.html
zip update.zip *.html
#tar cf --no-xattrs update.tar *html
scp update.zip linux-bastion.tudelft.nl:

#if [ "$1" == "-f" ]; then
#scp figs/*{png,jpg} gsteele@nsweb.tn.tudelft.nl:public_html/SQUID_practicum/figs
#fi

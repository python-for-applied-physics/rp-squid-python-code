---
jupyter:
  jupytext:
    formats: ipynb,md
    text_representation:
      extension: .md
      format_name: markdown
      format_version: '1.3'
      jupytext_version: 1.14.5
  kernelspec:
    display_name: Python 3 (ipykernel)
    language: python
    name: python3
---

# Instructional Videos SQUID Practicum

<a href="https://nsweb.tn.tudelft.nl/~gsteele/SQUID_practicum/videos/Liquid%20Nitrogen%20Safety.mp4">Liquid Nitrogen Safety</a>
* Also always wear gloves when handling the probe when it comes out of the liquid nitrogen. 
* NEVER touch the probe with your bare hands until there is no more ice forming on it!

<a href="https://nsweb.tn.tudelft.nl/~gsteele/SQUID_practicum/videos/Hardware_Introduction.mp4">Hardware introduction</a>
* An introduction to the MrSQUID hardware

<a href="https://nsweb.tn.tudelft.nl/~gsteele/SQUID_practicum/videos/Measurement_Software_first_run.mp4">Software introduction</a>
* Starting up the software and configuring it for your round
* Overview of the software programs and how to run them

<a href="https://nsweb.tn.tudelft.nl/~gsteele/SQUID_practicum/videos/Receiving_LN2_and_Measure_prope_IV.mp4">
Cooling the probe down and measuring superconducting IVs</a>

* Your first experiment: measuring IVs while cooling down
* Watch what happens when SQUID becomes superconducting
    
<a href="https://nsweb.tn.tudelft.nl/~gsteele/SQUID_practicum/videos/Probe_WarmUP_and_Taking_Temperature_Trace.mp4">Temperature  dependence</a>
* Measuring the superconducting transition while logging the sensor temperature when warming up

<a href="https://nsweb.tn.tudelft.nl/~gsteele/SQUID_practicum/videos/Flux_Oscillations_in_V_Phi_display.mp4">Quantum Interference</a>
* Using flux modulation mode to observe quantum interference

<a href="https://nsweb.tn.tudelft.nl/~gsteele/SQUID_practicum/videos/documenting_and_analysing_your_data.mp4">Documenting and analysing your data</a>

Important points from video:

* 4 levels of documentation:
    * Level 0: Use “Plot all my data” to get an overview of the data you’ve taken
    * Level 1: A “Daily summary” notebook for each day you work in the lab in which you describe in words what you did, and show plots of the representative data you took
    * Level 2: Analysis notebooks where you analyze the data in more detail, making your own plots and starting to draw conclusions
    * Level 3: Report plots should all be generated by one notebook, which you also submit with your report
* All files / notebooks / etc should be in the Teams folder
    * Sync to your own hard drive if you want too using one note 

<a href="https://nsweb.tn.tudelft.nl/~gsteele/SQUID_practicum/videos/End%20of%20session%20+%20leaving%20room.mp4"> End of session and leaving room </a>
* How to leave the setup when you are done

<a href="https://nsweb.tn.tudelft.nl/~gsteele/SQUID_practicum/videos/squid_practicum_teams_and_remote_desktop.mp4">Using Teams and connecting with Remote Desktop</a>

Notes on this video:
* Change with respect to video instructions: submit your reports only via brightspace, not via Teams
* Remote desktop:
    * Computer name: TUD204580
    * Username: TUD204580\localadmin
    * Password: see paper taped onto screen of measurement computer
    
Link to gallery with all videos:

https://nsweb.tn.tudelft.nl/~gsteele/SQUID_practicum/videos/

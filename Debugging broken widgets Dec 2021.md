---
jupyter:
  jupytext:
    formats: ipynb,md
    text_representation:
      extension: .md
      format_name: markdown
      format_version: '1.3'
      jupytext_version: 1.13.6
  kernelspec:
    display_name: Python 3 (ipykernel)
    language: python
    name: python3
---

# What we used to use

But no longer works with more recent versions of ipython kernel....

https://githubmemory.com/repo/kafonek/ipython_blocking/issues/11

```python
!jupyter --version
```

```python
import IPython
do_one_iteration = IPython.get_ipython().kernel.do_one_iteration
await do_one_iteration()
```

```python
import ipywidgets as widgets
from time import sleep
import IPython
#import asyncio

import nest_asyncio
nest_asyncio.apply()
do_one_iteration = IPython.get_ipython().kernel.do_one_iteration
#loop = asyncio.get_running_loop()

w = widgets.ToggleButton()
display(w)

i=0
while True:
    # This one gives: "asyncio.run() cannot be called from a running event loop"
    # asyncio.run(do_one_iteration())
    
    # This one gives "QueueEmpty:"
    await do_one_iteration()
    
    # the original one that worked, but now gives coroutine 
    # 'Kernel.do_one_iteration' was never awaited
    # and then updates are not propagated 
    # do_one_iteration()
    
    # This does not appear to be the solution results in 
    # RuntimeError: This event loop is already running
    #task = loop.create_task(do_one_iteration())
    #loop.run_until_complete(task)
  
    print(i, w.value, end="\r")
    w.decription = str(i)
    sleep(0.5)
    i+=1
```

```python
import ipywidgets as widgets
from time import sleep
import IPython
import asyncio

do_one_iteration = IPython.get_ipython().kernel.do_one_iteration
loop = asyncio.get_running_loop()

w = widgets.ToggleButton()
display(w)

i=0
while True:
    do_one_iteration()
    print(i, w.value, end="\r")
    w.decription = str(i)
    sleep(0.5)
    i+=1
```

OK, there is a suggestion that it should be possible still to run `do_one_iteration()`:

https://github.com/ipython/ipykernel/issues/825

https://github.com/ipython/ipykernel/pull/830

> I think the problem is that do_one_iteration is now a coroutine, so you can't expect to call it and get a response immediately. However, you can make it run synchronously again with asyncio.
>
>A similar problem (I think) for the Tk was solved here: #830.

https://github.com/ipython/ipykernel/issues/742

https://github.com/spyder-ide/spyder/issues/17024

> Well you can't because then the loops would be nested. Unless you use https://github.com/erdewit/nest_asyncio. But that might be a hard sell for ipykernel. I think the entrire tk eventloop has to be rewritten. THe solutions I see are to:
>
> Force ipykernel<6
> Disable tk backend

It seems like this has been done? Ah, but it will only be deployed as a fix for release 6.5 of ipykernel. I guess though that I could install it from source and see if things get fixed? 

And then I guess that 


# Solution with some help from Anton

```python
%gui asyncio
import asyncio
import ipywidgets as widgets

button = widgets.ToggleButton()
display(button)
text = widgets.Text()
display(text)
text.value= str(button.value)

stop_button = widgets.ToggleButton()
stop_button.description = "Stop"
display(stop_button)

async def f():   
    i=0
    while True:
        i += 1
        text.value = str(i) + " " + str(button.value)
        print(text.value + "    \r", end="")
        await asyncio.sleep(0.2)
        if stop_button.value == True:
            return

asyncio.create_task(f());
```

# Oh noooo! PyRPL uses QT GUI loop that kills %gui asyncio

```python
%gui qt5
```

```python
import pyrpl
```

```python
import asyncio
import ipywidgets as widgets
import qasync

button = widgets.ToggleButton()
display(button)
text = widgets.Text()
display(text)
text.value= str(button.value)

stop_button = widgets.ToggleButton()
stop_button.description = "Stop"
display(stop_button)

async def f():
    global task_running
    try:
        if task_running:
            return
    except NameError:
        pass
    task_running = True    
    i=0
    try:
        while True:
            i += 1
            text.value = str(i) + " " + str(button.value)
            await asyncio.sleep(0.2)
            if stop_button.value == True:
                return
    finally: # also executed even on retrun
        task_running = False
        
# asyncio.create_task(f());

loop = qasync.QEventLoop(pyrpl.APP)
asyncio.set_event_loop(loop)
asyncio.create_task(f());
```

# Try to reproduce a minimal example of the above without PyRPL

```python
%gui qt5
```

```python
import asyncio
import ipywidgets as widgets
import qasync

import IPython
do_one_iteration = IPython.get_ipython().kernel.do_one_iteration

button = widgets.ToggleButton()
display(button)
text = widgets.Text()
display(text)
text.value= str(button.value)

stop_button = widgets.ToggleButton()
stop_button.description = "Stop"
display(stop_button)

i=0
async def f(): 
    global i
    while True:
        i += 1
        text.value = str(i) + " " + str(button.value)
        print(text.value + "    \r", end="")
        await asyncio.sleep(0.2)
#        await do_one_iteration()
        if stop_button.value == True:
            return

from qtpy import QtWidgets
APP = QtWidgets.QApplication.instance()

loop = qasync.QEventLoop(APP)
asyncio.set_event_loop(loop)
asyncio.create_task(f());
```

# Try alternatives to `create_task()`

Had a look at what pyrpl does. This is intersting:

```
LOOP = quamash.QEventLoop() # Since tasks scheduled in this loop seem to
# fall in the standard QEventLoop, and we never explicitly ask to run this
# loop, it might seem useless to send all tasks to LOOP, however, a task
# scheduled in the default loop seem to never get executed with IPython
# kernel integration.
```

And they use only calls like this:

```
    await asyncio.sleep(time_s, loop=LOOP)
```

Maybe I could try using the `loop=` option with `create_task`? Apparently not: 

```
TypeError: create_task() got an unexpected keyword argument 'loop'
```

What about `run_until_complete`? 

```python
%gui qt5
```

```python
import asyncio
import ipywidgets as widgets
import qasync

button = widgets.ToggleButton()
display(button)
text = widgets.Text()
display(text)
text.value= str(button.value)

stop_button = widgets.ToggleButton()
stop_button.description = "Stop"
display(stop_button)

i=0 
async def f(): 
    global i
    while True:
        i += 1
        text.value = str(i) + " " + str(button.value)
        print(text.value + "    \r", end="")
        await asyncio.sleep(0.2)
        if stop_button.value == True:
            return

from qtpy import QtWidgets
APP = QtWidgets.QApplication.instance()

loop = qasync.QEventLoop(APP)
#asyncio.create_task(f(), loop=loop);
loop.run_until_complete(f())
```

Nope. 

Maybe try 

```python
%gui qt5
```

```python
import asyncio
import ipywidgets as widgets
import qasync

button = widgets.ToggleButton()
display(button)
text = widgets.Text()
display(text)
text.value= str(button.value)

stop_button = widgets.ToggleButton()
stop_button.description = "Stop"
display(stop_button)

i=0 
async def f(): 
    global i
    while True:
        i += 1
        text.value = str(i) + " " + str(button.value)
        print(text.value + "    \r", end="")
        await asyncio.sleep(0.2)
        if stop_button.value == True:
            return

from qtpy import QtWidgets
APP = QtWidgets.QApplication.instance()

loop = qasync.QEventLoop(APP)
#asyncio.create_task(f(), loop=loop);
#loop.run_until_complete(f())
asyncio.ensure_future(f(), loop=loop)
```

I get:

```
<Task pending name='Task-3' coro=<f() running at /var/folders/5t/sp26pw315mnbcfyyt9l9bvpwqb6r8t/T/ipykernel_92911/2133535786.py:16>>
```

And GUI settings never update. 


# Do widget changes ever propagate with `%gui qt5`? 

Answer is yes, once cell is executed. 

```python
%gui qt5
```

```python
import ipywidgets as widgets
stop_button = widgets.ToggleButton()
stop_button.description = "Stop"
display(stop_button)
```

```python
print(stop_button.value, "\r", end="")
```

# The final solution


Turns out there was a solution, needed `nest_asyncio`. 

```python
import ipywidgets as widgets
import time

button = widgets.ToggleButton()
display(button)
text = widgets.Text()
display(text)
text.value= str(button.value)

stop_button = widgets.ToggleButton()
stop_button.description = "Stop"
display(stop_button)

import nest_asyncio
nest_asyncio.apply()
import IPython
do_one_iteration = IPython.get_ipython().kernel.do_one_iteration

i=0
while True:
    i += 1
    text.value = str(i) + " " + str(button.value)
    print(text.value + "    \r", end="")
    time.sleep(0.2)
    await do_one_iteration()
    if stop_button.value == True:
        break
```

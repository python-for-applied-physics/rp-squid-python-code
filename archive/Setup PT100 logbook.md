---
jupyter:
  jupytext:
    formats: ipynb,md
    text_representation:
      extension: .md
      format_name: markdown
      format_version: '1.2'
      jupytext_version: 1.3.0
  kernelspec:
    display_name: Python 3
    language: python
    name: python3
---

```python
from pyrpl import Pyrpl
from time import sleep
import matplotlib.pyplot as plt
import numpy as np
```

```python
HOSTNAME = "rp-f06897.local"
config_file = "/Users/gsteele/pyrpl_user_dir/config/test.yml"

# Looking at the source, specifing config overrides the stupid GUI startup...
p = Pyrpl(hostname=HOSTNAME,config=config_file)

r = p.rp
s = r.scope
```

Annoying: even with the above options, stupid GUI still opens. Need to figure out how to get rid of this.

```python
r.scope.voltage_in1
```

Question: what units is that? If it's volts, it's pretty big!

```python
#help(r.scope.voltage_in1)
```

OK, that is not so useful. I think I'm just going to have to clone the repo and start poking in the code myself...

It seems like it should be in volts? 

```
voltage_in1 = FloatRegister(0x154, bits=14, norm=2 ** 13,
                                doc="in1 current value [volts]")
```

I will double check in the RP software itself

Interesting question: can do things like set the signal generator here and then view what comes out in the built-in RP  software? 

```python
asg = r.asg0
asg.output_direct = 'out1'
a = 1
asg.setup(waveform='sin', frequency=20, amplitude=a*1.1, offset=0, trigger_source='immediately')
```

Yes, I can. Super handy!

```python
def get_traces(avg=True):
    s.input1 = 'in1'
    s.input2 = 'in2'
    s.decimation = 1024
    s.average = avg
    s.trigger_source = 'immediately'
    s._start_acquisition()
    sleep(s.duration)
    c1,c2 = s._get_curve()
    t = s.times
    return t, c1, c2
```

Acquisition does look a bit strange though...

```python
asg = r.asg0
asg.output_direct = 'out1'
a = 0.5
asg.setup(waveform='sin', frequency=20, amplitude=a, offset=0, trigger_source='immediately')
```

```python
t, c1, _ = get_traces()
plt.subplots(figsize=(16,6))
plt.plot(t,c1)
plt.axhline(a, ls=':', c='grey')
plt.axhline(-a, ls=':', c='grey')
print("Peak to peak: ", (np.max(c1)-np.min(c1)))
print("Average: ", np.average(c1))
```

<!-- #region -->
Strange, now the offset and gain problems seem to be solved? 

OK, after closing my laptop, the connection reset.  Now when I run the acquisition with a 1 V amplitude, I get a peak to peak of:

```
Peak to peak:  0.9140625
```

Let's try starting the oscilloscope software of the RP, closing it, and then  doing the same acquisition agian.

OK, now I'm getting a different PTP reading:


```
Peak to peak:  0.983154296875
```

Now, let's try reloading the RP object and re-running the code: 

```
Peak to peak:  0.9150390625
```

OK, weird. Now open and close scope application via the RP web interface, and repeat:

```
Peak to peak:  0.9832763671875
```

And now also I'm getting sometimes some weird jumps in the trace. I 

Let's try power cycling the RP and then rerunning the code above. OK, this does not work, I've lost the connection and need to re-establish it by re-initialising the object above.  

Now my peak-to-peak values are back to the low values again: 

```
Peak to peak:  0.9149169921875
```

but also the strange trigger jumps are now gone in the trace. 

<!-- #endregion -->

Let's try a live update:

```python
from bokeh.plotting import figure, show
from bokeh.io import output_notebook, push_notebook
from bokeh.layouts import column
from bokeh.models import ColumnDataSource, Toggle, Range1d
output_notebook()
```

```python
source = ColumnDataSource()
p = figure(plot_height=300, plot_width=900,toolbar_location=None)
p.line('x', 'y', source=source)

def update_display():
    t,c1,_ = get_traces()
    source.data = dict(x=t,y=c1)   

update_display()
target = show(p, notebook_handle=True)

while True:
    update_display()
    push_notebook(handle=target)
    
```

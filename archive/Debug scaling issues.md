---
jupyter:
  jupytext:
    formats: ipynb,md
    text_representation:
      extension: .md
      format_name: markdown
      format_version: '1.2'
      jupytext_version: 1.3.0
  kernelspec:
    display_name: Python 3
    language: python
    name: python3
---

# Illustrate and debug scaling issues

I'll also try the updated code from Samuel:

https://github.com/lneuhaus/pyrpl/issues/396

We also have a gitlab issue for the scaling problems:

https://github.com/lneuhaus/pyrpl/issues/398

Of course, I can always fudge in a rescaling factor myself. But it would be good to figure out where it comes from. 

```python
from pyrpl import Pyrpl
from time import sleep
import matplotlib.pyplot as plt
import numpy as np
```

```python
HOSTNAME = "rp-f06897.local"

p = Pyrpl(hostname=HOSTNAME, config='test',gui=False)

r = p.rp
s = r.scope
```

```python
# I've got an SMA cable looping the output of generator 1 into the input of digitizer 1

asg = r.asg0
asg.output_direct = 'out1'
a = 1
asg.setup(waveform='sin', frequency=20, amplitude=a, offset=0, trigger_source='immediately')

s.input1 = 'in1'
s.input2 = 'in2'
s.decimation = 1024 # or s.duration =0.01
s.average = True
s.trigger_source = 'immediately'

s._start_acquisition()
sleep(s.duration)
c1,_= s._get_curve()

print("Max: ", np.max(c1))
print("Min: ", np.min(c1))
print("PTP: ", np.max(c1)-np.min(c1))
print("Offset: ", np.average(c1))

plt.plot(c1)
plt.ylim(-1.1,1.1)
plt.axhline(-1,ls=':', c='grey')
plt.axhline(1,ls=':', c='grey')
plt.show()
```

```python

```

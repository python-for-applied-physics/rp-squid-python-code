---
jupyter:
  jupytext:
    formats: ipynb,md
    text_representation:
      extension: .md
      format_name: markdown
      format_version: '1.2'
      jupytext_version: 1.6.0
  kernelspec:
    display_name: Python 3
    language: python
    name: python3
---

Following:

https://github.com/gsteele13/gary-misc-notebooks/blob/master/Test%20ADALM1000.ipynb

```python
import pysmu
import numpy as np
import matplotlib.pyplot as plt
from time import time
import time
```

```python
session = pysmu.Session()
```

```python
dev =  session.devices[0]
chA = dev.channels["A"]
```

```python
print(session.devices)
print(len(session.devices))
print(session.devices[0])
```

```python
# Put channel in "source current, measure voltage"
chA.mode = pysmu.Mode.SIMV
```

```python
# Set the current to 4 mA? 
chA.constant(2e-3)
```

```python
# Get 10 samples
chA.get_samples(10)
```

```python
N =  1e4
t0 = time()
voltage = np.array(chA.get_samples(N))[:,0]
t1 = time()
print("%.1f seconds" % (t1-t0))
print("%e sec per point" % ((t1-t0)/N))
plt.plot(voltage)
plt.show()
```

Measured voltage for a 4 mA current is about 4.235 V for the PT1000 sensor in the room right now.

The resistance I measure with the DMM is 1078. 

Does that make sense? 

```python
print(4e-3*1078)
```

OK, a bit stranget it is a bit off. Which do I trust? 


The PT1000 has a resistance of 1000 ohms at 0 degrees C, and a first order calibration extrapolates to zero resistance at zero temperature.

```python
T_in_K = 273/1000*1078
T_in_C = T_in_K - 273
print(T_in_K)
print(T_in_C)
```

That seems about right. 

Let's check the ALM assuming 4 mA:

```python
print(4.24/4e-3*273/1000)
print(4.24/4e-3*273/1000-273)
```

Seems a bit cold? I wonder what the DAC steps are in the sourcing mode of the ALM1000?


Maybe we need to calibrate some offsets and gains.

Let's try an IV:

```python
I = np.linspace(0,4e-3,100)
V = []
N = 10
for i in I:
    chA.constant(i)
    #time.sleep(0.1)
    print(".",end="")
    chA.get_samples(20) # need to clear the buffer?
    v = np.array(chA.get_samples(N))[:,0]
    V.append(v)
```

```python
plt.plot(I,V)
```

Looks pretty linear. Maybe it's a gain calibration problem? 


**In the meantime, had another dead kernel!!! This is going to be a problem...probably should go back to the arduino...**

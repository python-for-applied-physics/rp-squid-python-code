# This needs to be run in an old jupyter environment because the latest 
# version of jupyter broke contrib_nbextensions on purpose...
#
# ModuleNotFoundError: No module named 'jupyter_contrib_nbextensions'
#
# An old env that contains working nbextensions
# conda activate myenv

jupyter nbconvert --to html_embed "TN2513-P SQUID Practicum Manual.ipynb" --output-dir pdfs
jupyter nbconvert --to html_embed "Information about TN2513-P Squid Practicum.ipynb" --output-dir pdfs
jupyter nbconvert --to html_embed "Report Writing Guidelines and Advice.ipynb" --output-dir pdfs
jupyter nbconvert --to html_embed "Instructional Videos SQUID Practicum.ipynb" --output-dir pdfs

---
jupyter:
  jupytext:
    formats: ipynb,md
    text_representation:
      extension: .md
      format_name: markdown
      format_version: '1.3'
      jupytext_version: 1.13.6
  kernelspec:
    display_name: Python 3 (ipykernel)
    language: python
    name: python3
---

# Report Writing Advice and Guidelines

<!-- #region toc=true -->
<h1>Table of Contents<span class="tocSkip"></span></h1>
<div class="toc"><ul class="toc-item"><li><span><a href="#Report-Writing-Advice-and-Guidelines" data-toc-modified-id="Report-Writing-Advice-and-Guidelines-1">Report Writing Advice and Guidelines</a></span><ul class="toc-item"><li><ul class="toc-item"><li><span><a href="#Structure-of-your-report" data-toc-modified-id="Structure-of-your-report-1.0.1">Structure of your report</a></span><ul class="toc-item"><li><span><a href="#Abstract" data-toc-modified-id="Abstract-1.0.1.1">Abstract</a></span></li><li><span><a href="#Introduction" data-toc-modified-id="Introduction-1.0.1.2">Introduction</a></span></li><li><span><a href="#Theory-and-background" data-toc-modified-id="Theory-and-background-1.0.1.3">Theory and background</a></span></li><li><span><a href="#Methods" data-toc-modified-id="Methods-1.0.1.4">Methods</a></span></li><li><span><a href="#Results-&amp;-Discussion" data-toc-modified-id="Results-&amp;-Discussion-1.0.1.5">Results &amp; Discussion</a></span></li><li><span><a href="#Summary-and-outlook" data-toc-modified-id="Summary-and-outlook-1.0.1.6">Summary and outlook</a></span></li></ul></li><li><span><a href="#Report-Writing-Style-Expectations-and-Tips" data-toc-modified-id="Report-Writing-Style-Expectations-and-Tips-1.0.2">Report Writing Style Expectations and Tips</a></span><ul class="toc-item"><li><span><a href="#How-to-write-a-paragraph-in-scientific-writing" data-toc-modified-id="How-to-write-a-paragraph-in-scientific-writing-1.0.2.1">How to write a paragraph in scientific writing</a></span></li><li><span><a href="#LaTeX" data-toc-modified-id="LaTeX-1.0.2.2">LaTeX</a></span></li><li><span><a href="#Math-symbols-and-subscripts" data-toc-modified-id="Math-symbols-and-subscripts-1.0.2.3">Math symbols and subscripts</a></span></li><li><span><a href="#Appropriate-scientific-language-and-statements" data-toc-modified-id="Appropriate-scientific-language-and-statements-1.0.2.4">Appropriate scientific language and statements</a></span></li><li><span><a href="#Figures" data-toc-modified-id="Figures-1.0.2.5">Figures</a></span></li><li><span><a href="#Bullet-point-lists?" data-toc-modified-id="Bullet-point-lists?-1.0.2.6">Bullet point lists?</a></span></li><li><span><a href="#Error-and-estimated-precision" data-toc-modified-id="Error-and-estimated-precision-1.0.2.7">Error and estimated precision</a></span></li><li><span><a href="#Explain-how-you-calculate-your-numberes" data-toc-modified-id="Explain-how-you-calculate-your-numberes-1.0.2.8">Explain how you calculate your numberes</a></span></li></ul></li></ul></li></ul></li></ul></div>
<!-- #endregion -->

<!-- #region -->
### Structure of your report 

Your report should follow the following overall general structure:

#### Abstract 

1-2 sentences background about the experiment and why it is interesting. 2-3 sentences describing objectively what you did, 2-3 sentences explaining to the reader what results were obtained and what conclusions you can draw from them. 1 sentence describing an outlook of what your results mean for future experiments. 

####  Introduction

General background information related to the experiments you will do, “painting a backdrop” to your experiment, formulation of the research questions and what you hope to learn from them

You will  formulate research questions at the start, but feel free to deviate from those in your report, they are only a starting point. The research questions in your report should be motivated in the end by the conclusions you can draw from your results

#### Theory and background

The theoretical concepts that were required to understand the experimental results. Here, you should think of things that a typical 2nd year student may not yet already know, along with some review of things that are important that they may need to be reminded of

#### Methods

A technical description of the techniques you used for performing the experiments and analyzing the data. How was the data measured? What type of analysis techniques did you use? How did you perform the fitting? What procedure did you follow to extract quantitative numbers from your results for things like the critical current, the critical temperature, etc? Did you perform any data processing like averaging / filtering?  

#### Results & Discussion

This is a “Message” oriented chapter. Here you present selected plots and figures to convey a message. 

Do not separate the results from the discussion, they need to be kept together! 

First, divide this chapter into sections based on the type of experiment and give the sections titles based on what they are about, not what you did. For example: “Observing superconducivity” is a message-based section title. Reading this, the reader knows why they are reading this section. “IV Curve” does not tell a message: it only says what is in the section, not why the reader should read it. 

In some sections, you may want to analyze different aspects of that sub-experiment: for this you should use subsections. For example, a subsection of “Observing superconductivity in the SQUID probe” could be “Determining resistance of the superconducting state”, in which you extract the low bias slope of your observed IV curves using a fit. Or “Determining the critical current of the junction”

**How to write results & discussion:** You should base your writing around your figures. First, decide what data you want to present, why you want to present it (what is the “message”), and then make you figure. Once you have the figure, you write:

* First, a caption. The first sentence of the caption is the message, the rest of the caption describes what is shown in the figure adding the important information the reader needs to know in a concise way.

Then, you write three paragraphs (can be merged, but better to separate them at first so it is clear):

* Paragraph 1: Describe what the data is, reminding the reader of any important details of how it was measured that you will need when you provide your interpretation / discussion of the data. This paragraph is objective. 
* Paragraph 2: Describe what you see in the data. What happens? Are there any trends that are important? Is there something important to highlight to the reader that you will then discuss when you add your interpretation of the data? 
* Paragraph 3: Here, you will provide your own subjective interpretation of the data in the figure and try to convince the reader that your interpretation is correct. You should provide arguments for why you interpret the data in the way you do, and ultimately the reader should finish reading this paragraph and be convinced of your interpretation.

Note the different natures of these paragraphs: 

* Paragraphs 1 and 2 are “objective”, you should avoid the use of “we” as much as possible
* Paragraph 3 is subjective, it is your personal interpretation, it is OK to “we” a bit here in your writing:
* “We interpret the deviation in the measurement of critical temperature as …"

#### Summary and outlook

This should contain:

* 1 paragraph summarising your results again, emphasizing what you conclusions you were able to draw
* 1-2 paragraphs discussion about what your results mean for future experiments:
  * Are there things that should be investigated further? 
  * Are there ideas that came out of your report for new experiments to do? 



### Report Writing Style Expectations and Tips

For your report, we have certain expectations. We specifically write these down since these are general good practices for scientific writing. 

#### How to write a paragraph in scientific writing

Paragraphs: In scientific writing, it is good to make sure that your paragraphs have a proper formal structure

* Gary learned this as the “hamburger paragraph”:
  * https://thisreadingmama.com/paragraph-writing-main-idea-details/
* Hamburger paragraphs (with different names) are key to proper academic writing:
  * https://www.scribbr.com/research-paper/paragraph-structure/


#### LaTeX 

We strongly advise you to use LaTeX when writing using the TU Delft “Report” Template. This seems to no longer exist on the TU Delft website (good old TU Delft...) but can be found on Overleaf here:

https://www.overleaf.com/latex/templates/tud-report/qrntwbrqpckw

Overleaf (a online LaTex editor) is handy in general, and even mega-handy for collaborative writing, especially when you're writing your report and have question about text, figure placement etc. When you decide to use Overleaf please drop the link in your channel and pin it ("vastmaken"). You can pin your message using the three dots (...) button on the top-right of your post. 

We have TU Delft licences now for overleaf, and I strongly encourage you to use it. 


#### Math symbols and subscripts

* Write all your sub- and superscripts in non-cursive form (not in Italic). 
    * In Latex you can use one of the following commands to fix the problem where the equations are automatically written in Italic:
        * \mbox{}
        * \text{}
        * \textrm{}
        * \mathrm{}
            * Example: 		`$I_{\mathrm{c}}$`	which gets rendered as 	$I_{\rm{c}}$
    
#### Appropriate scientific language and statements

Write using precise scientific language!

* Try to be precise and don’t write sentences like: “it is very cold”. Instead, write quantitative stuff when applicable (e.g. “The device has been cooled down to liquid nitrogen temperature (~77K)” or “The temperature of the device is 123.45 K”
    * Use decimal points ( . ) and not commas ( , ), e.g. “123.456 K”
* If you know the error of the quantity you are quoting, choose one (or two) significant digits for your error value and quote the value to the same precision (number of decimal places) as the resulting error value
* If you do not have sufficient data to make a meaningful statement about the error, choose a number of digits you think is appropriate and provide a short discussion of how you made this choice
* Avoid writing that stuff is “hard”, “difficult” or “easy”
  
#### Figures

Figure tips:

##### PDF only, no PNG / JPG / etc!

* Do not use screenshots for plots of your data! Use PDFs generated by your analysis software (eg. Python)
    * Screenshots for illustrating details of how you used the software are OK, if you feel it is useful / important to explain how you used the software
    
    
* Save figures as .pdf file and not .png (or .jpeg). PDF-figures in Latex/pdf generated reports will look sharper. In python: 
    * fig.savefig("fig.pdf")
    
    
* Pictures from the experiment and of your exact setup are strongly encouraged, and of course can be included as JPG / PNG in your report
    
##### Zoom to make clear what aspect you want the reader to look at
    
* When plotting your data for discussion, chose a “zoomed” axis range if it helps explain your point
    * For example: if you took a data trace up to 300K and want to discuss the temperature dependence of the resistance, it might make sense to include the temperature range you measured in the plot
    * But if you want to discuss the transition temperature, it makes more sense to restrict your plot to a range of temperature near $T_{\rm c}$
    
##### Labels! 

* Label all your axes with all of the following:
    * A few (1-3 word) description (eg. Bias current)
    * A mathematical symbol you define in the text (eg. $I_{\rm b}$)
    * A unit (eg. A for Amps)
    
##### Legends

* Use legends when plotting multiple data sets with 
    * If possible, a mathematical symbol and value you define in the caption, eg “$I_{\rm b} = 1 $ A”
    * Optional short descriptions (1-2 word) in the legend text if they fit

##### Lines or points? 

Should I use lines or points or lines connected with points for plotting data?

* If you have “dense” data with lots of points (like an IV curve), and you have no analytical lines in your plot, then use lines
* If you have data and a fit / analytical formula in the same plot:
* Plot the data with symbols and no lines connecting them

If your data is dense, like an IV curve, use small dots (python matplotlib `plt.plot(x, y, ”.”)`)

* Plot the analytical formula, like a fit or a guide to the eye, with a line, mentioning the formula in the caption
* If you have “not very dense data”, such that the default symbol size would not overlap when you make the plot, then use only symbols
* These are most likely for data that you are “consolidating” from sequences of measurements, like say plotting a fit parameter vs temperature from raw datasets (I'm not sure if there really are any in this practicum, but it could come up depending on what you do for the data analysis). In this case, you most likely also have error bars for each data point, and these should also be included.

##### How to put figures in my report

* If you have two panels, put them side-by-side (matplotlib subplot(121)) and make them the full width of the text 
* If you have one figure, make at least +/- 60% of the text width
* Center all figures on the page
* Figure caption should be left justified, not centered

##### Font sizes!

Font in figures should be large enough for the reader to write it 
* If you measure it with a ruler, minimum font size should be 3 mm tall for a capital letter (9 point)
* Also think about how visible colours are to your reader as well

##### Make your own figures!!!! 

Do not include figures based on the supplied “automated” plotting routines in the main chapters of the report
* These are things that we provide for you to quickly be able to plot and analyze your data while you’re working
* For the figures in the report, we expect that you spend time to think about what the figure contents should be based on the message you want that figure to convey
* You are welcome to look at how we make these plots and reuse this code in your own plots, but the figures they make are not suitable for direct inclusion in the report
* There is also some code that shows you how to load and plot the data from the created data files
* As mentioned above, we do expect you to put a copy of the code you write for making the figures you include in the report in the appendeix
* If you make a claim in your text, always back it up with either data or a reference (book, paper, etc.)
* Write your research questions in the introduction chapter of your report

#### Bullet point lists?

You can write things your research questions as bullet points. This can be useful to provide a visually identifyable summary, but there should be some part of your report (preferably before or nearby the list) that explains a bit at least what the bullet points mean. 

#### Error and estimated precision

Whenever possible, include the uncertainty in the numbers you quote:  eg. 123.45 ± 0.05 Ω. You should include on "sigificant digit" in the error values, and quote the measured value to an accuracy of that significant digit. 

When quoting error values, you should explain where the precision estimate comes from. If you have a statistical origin of your uncertianty (like parameter error from a least-squares fit), you should state this (and include the code). If you get it from some other way of estimating it, explain in words what you did.  

#### Explain how you calculate your numberes

In your report, you must show, or tell, how you calculated your numbers or data when you discuss it.

* If this text because too large or interrupts the flow of your story significantly then you can include only a short description in your report main text, place the full details in your appendix or methods section, and reference it when discussion.
<!-- #endregion -->
